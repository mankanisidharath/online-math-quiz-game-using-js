// Variables
let playing = false;
let score = 0;
let timeremaining;
let countdown = 0;
let correctans;

//Required Elements
let startReset = document.getElementById("startReset");

//Event Registration
startReset.addEventListener('click', startResetGame);
document.getElementById("close").addEventListener('click', hideGameover);
document.getElementById("box1").addEventListener('click', processAnswer); // this problem
document.getElementById("box2").addEventListener('click', processAnswer);
document.getElementById("box3").addEventListener('click', processAnswer);
document.getElementById("box4").addEventListener('click', processAnswer);

//Helper Functions
function setText(id, text){
	document.getElementById(id).innerHTML = text;
}

function show(id){
		document.getElementById(id).style.display = 'block';
}

function hide(id){
		document.getElementById(id).style.display = 'none';
}

function hideGameover(){
	hide("pageHeight");
	hide("gameover");
	setText("scoreValue", "--");
}

function startResetGame(){
	if(playing){
		// game is on and you want to reset
		setText("startReset", "Start Game");
		show("start");
		hide("timeremaining");
		show("dummy");
		hide("question");
		setText("question", "Play");
		setText("scoreValue", "--");
		clearInterval(countdown);
		
	} else{
		// game is off and you want to start a new game
		setText("startReset", "Reset Game");
		show("timeremaining");
		hide("start");
		show("question");
		hide("dummy");
		score = 0;
		timeremaining = 60;
		setText("timeremainingvalue", timeremaining);
		startCountdown();
		generateQA();
	}
	playing = !playing;
}
var timeoutID;
var previous;
function startCountdown(){
	
	clearInterval(countdown);
	countdown = setInterval(function(){
		timeremaining -= 1;
		setText("timeremainingvalue", timeremaining);
		if(timeremaining <= 0){
			clearInterval(countdown);
			show("dummy");
			hide("question");
			if(score < 10){
				setText("finalScore", "0"+score);
			} else{
				setText("finalScore", score);
			}
			show("pageHeight");
			document.getElementById("gameover").style.display = 'flex';
			playing = false;
			setText("startReset", "Start game");
			show("start");
			hide("timeremaining");
		}
	}, 1000);

/*
	clearTimeout(timeoutID);
	clearTimeout(previous);
	function name(){
		timeremaining -= 1;
		setText("timeremainingvalue", timeremaining);
		if(timeremaining <= 0){
			clearTimeout(timeoutID);
			show("pageHeight");
			document.getElementById("gameover").style.display = 'flex';
			playing = false;
			setText("startReset", "Start game");
			show("start");
			hide("timeremaining");
		}
		clearTimeout(previous);
		previous = setTimeout(function(){
			name();
		}, 1000);
	}

	timeoutID = setTimeout(function(){
			name();
	}, 1000);

*/
}

function generateQA(){
	let no1 = Math.round(1 + Math.random() * 9);
	let no2 = Math.round(1 + Math.random() * 9);
	let choice = Math.round(Math.random() * 2);
	let correctposition = Math.round(1 + Math.random() * 3);
	var qstr;

	switch(choice){
		case 0: qstr = no1 + " x " + no2;
				correctans = no1 * no2;
				break;
		case 1: qstr = no1 + " + " + no2;
				correctans = no1 + no2;
				break;
		case 2: qstr = no1 + " - " + no2;
				correctans = no1 - no2;
				break;
	}
	setText("question", qstr);

	setText("box"+correctposition, correctans);
	let answers = [correctans];
	for(let i = 1; i<=4; i++){
			if(i!=correctposition){
				do{
					wronganswer = generateRandom() * generateRandom();
				}while(answers.indexOf(wronganswer) > -1);
				setText("box"+i, wronganswer);
			}
		}
	}

	function generateRandom(){
			return Math.round(1 + Math.random() * 9);
	}


function processAnswer(){
	
	if(playing) {	
		if(this.innerHTML == correctans){
			show("correct");
			hide("wrong");
			score++;
			if(score < 10){
				setText("scoreValue", "0"+score);
			}
			else{
				setText("scoreValue", score);
			}
		} else{
			show("wrong");
			hide("correct");
		}
		setTimeout(function(){
			hide("correct");
			hide("wrong");
		}, 1000);
		generateQA();
	}
	else{
		setText("start", "Please click the start button!");
		document.getElementById("start").style.color =  "#DC040F";
		setTimeout(function(){
			document.getElementById("start").style.color =  "#210124";
		}, 300);
		setTimeout(function(){
			document.getElementById("start").style.color =  "#DC040F";
		}, 500);
		setTimeout(function(){
			document.getElementById("start").style.color =  "#210124";
		}, 1200);
		setTimeout(function(){
			setText("start", "Start the quiz!");
		}, 4000);
	}
}

































